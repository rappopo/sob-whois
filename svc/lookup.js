const { DateTime } = require('rappopo-sob').Helper
const whois = require('whois')

module.exports = ({ sobr, sob}) => {
  return {
    settings: {
      webApi: {
        alias: {
          'GET /:id': 'query'
        }
      }
    },
    actions: {
      query (ctx) {
        return new Promise((resolve, reject) => {
          const { id } = ctx.params
          whois.lookup(id, (err, data) => {
            if (err) reject(err)
            else {
              resolve({
                ts: DateTime.utc().toISO(),
                query: id,
                result: data
              })
            }
          })
        })
      }
    }
  }
}
